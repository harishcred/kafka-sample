### Producer config
```yaml
sampleProducer:
  topic: customer
  brokers: localhost:9092
  sync: true
  value.serializer: org.apache.kafka.common.serialization.BytesSerializer
  retries: 0
  acks: all
```

Topic: Topic name
Brokers: Broker
Sync: true if you want to send message in sync (recommended)

# If you want to send bytes as a payload then use this, otherwise remove it.
value.serializer: org.apache.kafka.common.serialization.BytesSerializer

retries: How many times to retry if error
acks: all (kafka ack values)

### Consumer config
```yaml
sampleConsumer:
  topic: customer
  broker: localhost:9092
  group.id: 1235
```