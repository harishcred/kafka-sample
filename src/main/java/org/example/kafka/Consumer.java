package org.example.kafka;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.gitbub.devlibx.easy.helper.json.JsonUtils;
import io.gitbub.devlibx.easy.helper.map.StringObjectMap;
import io.gitbub.devlibx.easy.helper.yaml.YamlUtils;
import io.github.devlibx.easy.messaging.config.MessagingConfigs;
import io.github.devlibx.easy.messaging.kafka.module.MessagingKafkaModule;
import io.github.devlibx.easy.messaging.module.MessagingModule;
import io.github.devlibx.easy.messaging.service.IMessagingFactory;
import lombok.Data;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.BytesSerializer;
import org.slf4j.ILoggerFactory;

import java.util.UUID;
import java.util.logging.Logger;

public class Consumer {

    // Just a wrapper class to read YAML config for kafka
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class KafkaMessagingTestConfig {
        private MessagingConfigs messaging;
    }

    public static void main(String[] args) throws Exception {
        // Setup messaging factory - recommended using this code, as it will setup defaults.
        // You can create objects by yourself if you want
        KafkaMessagingTestConfig kafkaConfig = YamlUtils.readYaml("kafka_test_config.yml", KafkaMessagingTestConfig.class);
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(MessagingConfigs.class).toInstance(kafkaConfig.messaging);
            }
        }, new MessagingKafkaModule(), new MessagingModule());
        // ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG
        IMessagingFactory messagingFactory = injector.getInstance(IMessagingFactory.class);
        messagingFactory.initialized();

        // Get and start consuming data
        messagingFactory.getConsumer("sampleConsumer").ifPresent(consumer -> {
            consumer.start((message, metadata) -> {
                // Consumer event
                System.out.println(message.getClass());
                System.out.println(message);
            });
        });


        // Produce sample data
        messagingFactory.getProducer("sampleProducer").ifPresent(producer -> {
            for (int i = 0; i < 10000; i++) {

                // Produce event to Kafka
                Object toSend = StringObjectMap.of("input", UUID.randomUUID().toString());
                producer.send(UUID.randomUUID().toString(), JsonUtils.asJson(toSend).getBytes());

                // Sleep for Demo
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }

        });

        Thread.sleep(100000);
    }
}
